package test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import main.biblioModel.Livre;

public class BookTest {

	@Test
	public void livreTest() {
		Livre liv = new Livre("Samuel", "Beckett", "Editions de minuit", 1952, "En Attendant Godot", 0);
		assertEquals(liv.getNomAuteur(), "Beckett");
		assertEquals(liv.getPrennomAuteur(), "Samuel");
		assertEquals(liv.getEditeur(), "Editions de minuit");
		assertEquals(liv.getAnnee(), 1952);
		assertEquals(liv.getTitre(), "En Attendant Godot");
		assertEquals(liv.getId(), 0);
	}

	@Test
	public void livreTestWithoutArgs() {
		Livre liv = new Livre();
		assertEquals(liv.getNomAuteur(), null);
		assertEquals(liv.getPrennomAuteur(), null);
		assertEquals(liv.getEditeur(), null);
		assertEquals(liv.getAnnee(), -1);
		assertEquals(liv.getTitre(), null);
		assertEquals(liv.getId(), 0);

	}
}
