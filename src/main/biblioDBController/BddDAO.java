package main.biblioDBController;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import main.biblioModel.Livre;


public class BddDAO {
	
	private static Connection connec;

	public BddDAO () throws SQLException {
		
		Properties lp = new Properties();
		FileInputStream lFis = null;
		
		try {

			lFis = new FileInputStream("C:\\Eclipse\\The_DDDF_Jectpro\\WebContent\\WEB-INF\\db.properties");
			lp.load(lFis);
			String url = lp.getProperty("DB_URL");
			connec = DriverManager.getConnection(url);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//String url = "jdbc:sqlite:bibliotheque.sqlite";
		//connec = DriverManager.getConnection(url);
		
		Statement st = connec.createStatement();
		st.executeUpdate("CREATE TABLE IF NOT EXISTS  Livres " + "(id_livre INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ " titre TEXT NOT NULL," + " annee INT NOT NULL," + " editeur TEXT NOT NULL,"
				+ "nom_auteur TEXT NOT NULL, " + " prenom_auteur TEXT NOT NULL);");
	}


	// Recuperation de tous les livres dans la BDD
	
	public List<Livre> getLivres() throws SQLException{
		List<Livre> listl = new ArrayList<>();
		String titre;
		int annee;
		String editeur;
		String nom;
		String prenom;
		int id;
		
		PreparedStatement ps = connec.prepareStatement("SELECT * FROM Livres");
		ResultSet resultSetLivre = ps.executeQuery();

		while (resultSetLivre.next()) {
			Livre tempoBook;
			titre =resultSetLivre.getString("titre");
			annee =resultSetLivre.getInt("annee");
			editeur =resultSetLivre.getString("editeur");
			nom =resultSetLivre.getString("nom_auteur");
			prenom =resultSetLivre.getString("prenom_auteur");
			id =resultSetLivre.getInt("id_livre");
			tempoBook = new Livre(prenom,nom,editeur,annee,titre,id);
			listl.add(tempoBook);	
		}
		return listl;
	}
	
	public Livre getUnLivre(int id) throws SQLException{
		
		List<Livre> listl = new ArrayList<>();
		String titre;
		int annee;
		String editeur;
		String nom;
		String prenom;
		int id1;
		
		PreparedStatement ps = connec.prepareStatement("SELECT * FROM Livres WHERE id_livre=?");
		ps.setInt(1, id);
		ResultSet resultSetLivre = ps.executeQuery();

		while (resultSetLivre.next()) {
			Livre tempoBook;
			titre =resultSetLivre.getString("titre");
			annee =resultSetLivre.getInt("annee");
			editeur =resultSetLivre.getString("editeur");
			nom =resultSetLivre.getString("nom_auteur");
			prenom =resultSetLivre.getString("prenom_auteur");
			id1 =resultSetLivre.getInt("id_livre");
			tempoBook = new Livre(prenom,nom,editeur,annee,titre,id1);
			listl.add(tempoBook);	
		}
		Livre unLivre = listl.get(0);	
		return unLivre;
	}
	
	public void removeLivre(int id) throws SQLException {
		PreparedStatement ps = connec.prepareStatement("DELETE FROM Livres WHERE id_livre=?");
		ps.setInt(1, id);
		ps.executeUpdate();
	}
	
	public void newLivre(Livre li) throws SQLException {
		PreparedStatement ps = connec.prepareStatement(
						"INSERT INTO Livres (titre,  nom_auteur,prenom_auteur, annee,editeur) VALUES (?,?,?,?,?)");
		ps.setString(1, li.getTitre());
		ps.setString(2, li.getNomAuteur() );
		ps.setString(3, li.getPrennomAuteur());
		ps.setInt(4, li.getAnnee());
		ps.setString(5, li.getEditeur());
		ps.executeUpdate();
	}
	
		
}
