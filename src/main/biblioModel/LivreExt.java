
package main.biblioModel;

import java.util.HashMap;
import java.util.Map;

public class LivreExt {

	private int booksId;
	private String bookTitle;
	private int bookYear;
	private String editor;
	private String authorFirstName;
	private String authorName;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public LivreExt() {
	}

	public LivreExt(int booksId, String bookTitle, int bookYear, String editor, String authorFirstName,
			String authorName) {
		super();
		this.booksId = booksId;
		this.bookTitle = bookTitle;
		this.bookYear = bookYear;
		this.editor = editor;
		this.authorFirstName = authorFirstName;
		this.authorName = authorName;
	}

	public int getBooksId() {
		return booksId;
	}

	public void setBooksId(int booksId) {
		this.booksId = booksId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public int getBookYear() {
		return bookYear;
	}

	public void setBookYear(int bookYear) {
		this.bookYear = bookYear;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

}