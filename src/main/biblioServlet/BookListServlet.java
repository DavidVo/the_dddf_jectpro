package main.biblioServlet;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.biblioModel.Livre;
import main.biblioModel.LivreExt;

@WebServlet("/BookListServlet")
public class BookListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public BookListServlet() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	RestTemplate rt = new RestTemplate();
    	
    	String url = "http://192.168.1.25:8095/books";
    	//String url = "http://localhost:8080/biblio";
    	
    
    	// Recuperation de la liste de livres au format "exterieur"
		ResponseEntity<List<LivreExt>> tempoBookList = rt.exchange(url, 
				HttpMethod.GET,null, new ParameterizedTypeReference<List<LivreExt>>(){});
		List<LivreExt> bookListExt = tempoBookList.getBody();
    	
		// Transformation en notre format de livre
		List<Livre> bookList = new ArrayList<>();
		String pr;
		String nm;
		String ed;
		int an;
		String ti;
		int id;
		
		for(LivreExt eachBook:bookListExt) {
	    	pr = eachBook.getAuthorFirstName();
	    	nm = eachBook.getAuthorName();
	    	ed = eachBook.getEditor();
	    	an = eachBook.getBookYear();
	    	ti = eachBook.getBookTitle();
	    	id = eachBook.getBooksId();
	    	bookList.add(new Livre(pr,nm,ed,an,ti,id));
		};
    	
		/** Test Pour nous
		ResponseEntity<List<Livre>> tempoBookList = rt.exchange(url, 
				HttpMethod.GET,null, new ParameterizedTypeReference<List<Livre>>(){});
		List<Livre> bookListExt = tempoBookList.getBody();
    		**/
		
		//Envoie à notr jsp
		request.setAttribute("bookList" , bookList);
		this.getServletContext().getRequestDispatcher( "/bookList.jsp" ).forward( request, response );
	}

		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
