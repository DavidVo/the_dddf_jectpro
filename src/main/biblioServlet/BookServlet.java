package main.biblioServlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.biblioDBController.BddDAO;
import main.biblioModel.Livre;
import main.biblioModel.LivreExt;

@WebServlet(name = "/BookServlet",urlPatterns="/BookServlet")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
    	RestTemplate rt = new RestTemplate();
    	
    	// Recuperation du livre au format "exterieur"
    	
    	String url = "http://192.168.1.25:8095/books/" + request.getParameter("idBook");
    	LivreExt unLivreExt = rt.getForObject(url, LivreExt.class);
    	
    	/**
    	String url = "http://localhost:8080/biblio/" + request.getParameter("idBook");
    	LivreExt unLivreExt = rt.getForObject(url, LivreExt.class);
    	**/
    	
    	//Test Pour nous
    	//Livre unLivreExt = rt.getForObject(url, Livre.class);
    	

    	// Transformation en notre format de livre
    	String pr = unLivreExt.getAuthorFirstName();
    	String nm = unLivreExt.getAuthorName();
    	String ed = unLivreExt.getEditor();
    	int an = unLivreExt.getBookYear();
    	String ti = unLivreExt.getBookTitle();
    	int id = unLivreExt.getBooksId();



    	// Ajout du livre dans notre BD
		BddDAO conn;
		try {
			conn = new BddDAO();
			conn.newLivre(new Livre(pr, nm, ed,an,ti));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Envoie au .jsp

		request.setAttribute("Livre" , new Livre(pr,nm,ed,an,ti,id));

		this.getServletContext().getRequestDispatcher( "/book.jsp" ).forward( request, response );
	}


}
