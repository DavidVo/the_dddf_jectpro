package main.biblioApplication;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import main.biblioDBController.BddDAO;
import main.biblioModel.Livre;

@RestController
public class BiblioController {
	
	@GetMapping(value="/biblio", produces ="application/json")
	public List<Livre> getLivres() throws SQLException{
		List<Livre> ll = new ArrayList<>();
		BddDAO conn = new BddDAO();
		return ll = conn.getLivres();
	}
	
	@GetMapping(value="/biblio/{id}", produces="application/json")
	public Livre getLivre(@PathVariable("id") int n) throws SQLException {
		BddDAO conn = new BddDAO();
		Livre tempoLivre = conn.getUnLivre(n);
		conn.removeLivre(n);
		return tempoLivre;
	}
}
