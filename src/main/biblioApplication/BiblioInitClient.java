package main.biblioApplication;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import main.biblioDBController.BddDAO;
import main.biblioModel.Livre;


public class BiblioInitClient {

	public static void main(String[] args) {
		
		try {


			BddDAO conn = new BddDAO();
			conn.newLivre(new Livre("Orwell", "George", "Secker",1949,"1984"));
			conn.newLivre(new Livre("Bradbury", "Ray", "Ballantine",1953,"Fahrenheit 451"));
			conn.newLivre(new Livre("Herbert", "Frank", "Robert Laffont à fond dans le fond",1965,"Dune"));
			conn.newLivre(new Livre("Asimov", "Isaac", "Gnome Press",1957,"Fondation"));
			conn.newLivre(new Livre("Melville", "Herman", "Bentley",1851,"Moby-Dick"));
		
		} catch(Exception ex) { ex.printStackTrace();}
	}

}
