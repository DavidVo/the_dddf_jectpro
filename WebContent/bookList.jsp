<%@ page language="java" contentType="text/html; charset=ISO-8859-1"

	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file="/header.jsp"%>
<%@ include file="/footer.jsp"%>
<title>BookList</title>
<%@ include file="/header.jsp"%>
</head>
<%@ include file="/footer.jsp"%>
<body>
<%@ include file="/title.jsp"%>
	<h1>Liste de livre disponible</h1>
	<c:forEach items="${ bookList }" var="book">
		<div class="row">
			<div class="col-4">
				<div class="list-group" id="list-tab" role="tablist">
					<a class="list-group-item list-group-item-action">${book.titre}</a>
					<a class="list-group-item list-group-item-action">${book.annee}</a>
					<a class="list-group-item list-group-item-action">${book.editeur}</a>
					<a class="list-group-item list-group-item-action">${book.nomAuteur}</a>
					<a class="list-group-item list-group-item-action">${book.prennomAuteur}</a>
				</div>
			</div>
			<a href="BookServlet?idBook=${ book.id }">Retirer</a>
	</c:forEach>
	<%@ include file="/footer.jsp"%>
</body>
</html>